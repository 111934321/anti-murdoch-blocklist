Fork of: https://github.com/JimmyRecard/anti-murdoch-blocklist

# Anti Murdoch Blocklist

This is a very simple hosts based blocklist containing all of the websites that are either owned by News Corp or are under editorial control of News Corp or Rupert Murdoch.
Initial version based on https://en.wikipedia.org/wiki/List_of_assets_owned_by_News_Corp
Any hosts-based adblocker should be able to accept it as a source list.

Please remember to use [the RAW list](https://codeberg.org/111934321/anti-murdoch-blocklist/raw/branch/main/hosts.txt).

If you find that any site is missing or erranously listed, please open an issue to have it fixed.


See also:

[Bye Rupert Chrome Extension](https://chrome.google.com/webstore/detail/bye-rupert/ehdikikkfbfjjemfadgggcohkjoggoof)

[Bye Rupert Firefox Extension](https://addons.mozilla.org/en-US/firefox/addon/bye-rupert/)

[Similar Blocklist](https://github.com/suodrazah/murdoch_blocklist)